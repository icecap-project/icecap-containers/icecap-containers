{ lib, pkgs, configured }:

let 
  inherit (pkgs.dev) runCommand writeScript nukeReferences;
  inherit (pkgs.none.icecap) icecapSrc crateUtils elfUtils platUtils;
  inherit (pkgs.linux.icecap) linuxKernel uBoot nixosLite;

  inherit (configured)
    icecapFirmware icecapPlat selectIceCapPlatOr
    mkIceDL mkDynDLSpec mkLinuxRealm
    globalCrates;
  
in

rec {

  commonBootargs = [
    "earlycon=icecap_vmm"
    "console=hvc0"
    "loglevel=7"
  ];

  spec = mkLinuxRealm {
    kernel = linuxKernel.realm.kernel;
    bootargs = commonBootargs;
    initrd = ../vm-builder/alpine-icecap-guest-initrd.cpio.gz;
  };

  run = platUtils.${icecapPlat}.bundle {
    firmware = icecapFirmware.image;
    payload = icecapFirmware.mkDefaultPayload {
      linuxImage = linuxKernel.host.${icecapPlat}.kernel;
      bootargs = commonBootargs ++ [
        "spec=${spec}"
        "icecap-host=${pkgs.musl.icecap.icecap-host}/bin/icecap-host"
        "root=/dev/vda" "rw" #"rootfstype=9p" "rootflags=trans=virtio"
        "init=/init"
      ];
      initramfs = linuxKernel.host.${icecapPlat}.kernel;
      #initramfs = ../vm-builder/alpine-icecap-guest-initrd.cpio.gz;
    };
    platArgs = selectIceCapPlatOr {} {
      rpi4 = {
        extraBootPartitionCommands = ''
          ln -s ${spec} $out/spec.bin
        '';
      };
    };
  };
}
