# Creating Host and Guest Rootfs and initrd:

We use `Dockerfiles` to build custom host and guest VMs. Follow this guide to [install Docker](https://docs.docker.com/engine/install/) if necessary.

Once all the necessary requirements are available, run the following commands:
- Clone the Icecacp repository:

    ```
    git clone --recursive -b dev https://gitlab.com/arm-research/security/icecap/icecap-containers/icecap-containers.git
    ```
- Once you have a local copy of the Icecap source, run the following to generate the host and guest rootfs and initrd.

    ```
    cd vm-build
    ./create.sh all --guest-file Dockerfile.guest --host-file Dockerfile.host
    ```
## Custom Guest VM:

To Customize the guest VM, simply change [the guest Dockerfile](../vm-build/Dockerfile.guest) and rebuild the guest initrd using the script above.

In case the new custom guest needs additional/different kernel configuration, follow this [guide to build a new kernel](build-kernel.md). Once the build is done, modify this line in the [instance.nix](../nix/instance.nix) to point to the location of the new kernel.
```
spec = mkLinuxRealm {
    kernel = local_path_to_kernel;
    initrd = local_path_to_inirtd;
    ...
  };
```

